# Antenna Tracker
This is a minimal antenna tracker implementation for tracking MAVLink drones. It is implemented with the Zephyr RTOS platform, and designed to work on the Black Pill F401CE development board (alternate platforms may be supported in the future).

## Building
1. `git clone https://gitlab.com/amadoruavs/antenna-tracker`
2. `cd antenna-tracker`
3. `west update`
4. `west build -b blackpill_f401ce`
