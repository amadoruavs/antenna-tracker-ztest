#include "mavlink/common/mavlink.h"
#include "mavlink/mavlink_helpers.h"
#include "usb.h"
#include <stdio.h>
#include <string.h>
#include <zephyr.h>
#include <device.h>
#include <sys/printk.h>
#include <drivers/sensor.h>
#include <sys/ring_buffer.h>
#include <logging/log.h>
#include <kernel.h>

#include "mavlink.h"

struct k_timer heartbeat_timer;
struct k_timer gimbal_status_timer;
struct k_timer attitude_status_timer;

const struct device *usb;

int chan = MAVLINK_COMM_0;
mavlink_status_t rstatus;
mavlink_message_t rmsg;

uint8_t sysid_primary_control;
uint8_t compid_primary_control;
uint8_t sysid_secondary_control;
uint8_t compid_secondary_control;

uint8_t aps_sys_id;
uint8_t aps_comp_id;

float gimbal_q[4];
float gimbal_angular_vel_x;
float gimbal_angular_vel_y;
float gimbal_angular_vel_z;
uint32_t gimbal_failures = 0;

#define MAVLINK_BUFFER_LEN 2041
uint8_t buf[MAVLINK_BUFFER_LEN];

void set_mavlink_id(uint8_t sys_id, uint8_t comp_id) {
    aps_sys_id = sys_id;
    aps_comp_id = comp_id;
    printf("Set id to: (%d, %d)\n", sys_id, comp_id);
}

void set_mavlink_usb(const struct device *dev) {
    usb = dev;
}

uint8_t set_gimbal_attitude_info(float q[4], float angular_vel_x, float angular_vel_y, float angular_vel_z) {
    gimbal_q[0] = q[0];
    gimbal_q[1] = q[1];
    gimbal_q[2] = q[2];
    gimbal_q[3] = q[3];

    gimbal_angular_vel_x = angular_vel_x;
    gimbal_angular_vel_y = angular_vel_y;
    gimbal_angular_vel_z = angular_vel_z;

    return 0;
}

uint8_t send_heartbeat() {
    // printf("Send heartbeat: (%d, %d)\n", aps_sys_id, aps_comp_id);
    mavlink_message_t msg;
    mavlink_msg_heartbeat_pack(
            aps_sys_id, aps_comp_id,
            &msg,
            MAV_TYPE_ANTENNA_TRACKER,
            MAV_AUTOPILOT_INVALID,
            MAV_MODE_FLAG_SAFETY_ARMED, 0, MAV_STATE_ACTIVE);
    uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
    uint32_t send_len = uart_fifo_fill(usb, buf, len);
    if (send_len < len) {
        printf("Drop %d bytes\n", len - send_len);
    }
    return 0;
}

uint8_t send_gimbal_manager_info() {
    printf("Send gimbal manager info\n");
    mavlink_message_t msg;
    mavlink_msg_gimbal_manager_information_pack(
            aps_sys_id, aps_comp_id,
            &msg, k_uptime_get(),
            GIMBAL_MANAGER_CAP_FLAGS_HAS_PITCH_AXIS |
            GIMBAL_MANAGER_CAP_FLAGS_HAS_YAW_AXIS |
            GIMBAL_MANAGER_CAP_FLAGS_HAS_PITCH_LOCK |
            GIMBAL_MANAGER_CAP_FLAGS_HAS_YAW_LOCK |
            GIMBAL_MANAGER_CAP_FLAGS_SUPPORTS_INFINITE_YAW,
            aps_comp_id,
            0, 0,
            0, 1.57,
            0.0/0.0, 0.0/0.0);

    uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
    uint32_t send_len = uart_fifo_fill(usb, buf, len);
    if (send_len < len) {
        printf("Drop %d bytes", len - send_len);
    }
    return 0;
}

uint8_t send_gimbal_manager_status() {
    // printf("Send gimbal manager status\n");
    mavlink_message_t msg;
    mavlink_msg_gimbal_manager_status_pack(
            aps_sys_id, aps_comp_id,
            &msg, k_uptime_get(),
            aps_comp_id,
            GIMBAL_MANAGER_FLAGS_YAW_LOCK |
            GIMBAL_MANAGER_FLAGS_PITCH_LOCK,
            0, 0, 0, 0); // TODO: Add actual primary control logic

    uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
    uint32_t send_len = uart_fifo_fill(usb, buf, len);
    if (send_len < len) {
        printf("Drop %d bytes", len - send_len);
    }
    return 0;
}


uint8_t send_gimbal_device_attitude_status() {
    mavlink_message_t msg;
    mavlink_msg_gimbal_device_attitude_status_pack(
            aps_sys_id, aps_comp_id,
            &msg,
            0, 0,
            k_uptime_get(),
            GIMBAL_DEVICE_FLAGS_YAW_LOCK |
            GIMBAL_DEVICE_FLAGS_PITCH_LOCK,
            gimbal_q,
            gimbal_angular_vel_x,
            gimbal_angular_vel_y,
            gimbal_angular_vel_z, gimbal_failures);

    uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
    uint32_t send_len = uart_fifo_fill(usb, buf, len);
    if (send_len < len) {
        printf("Drop %d bytes", len - send_len);
    }
    return 0;
}

uint8_t send_ack(uint16_t command, uint8_t result, uint8_t target_sys, uint8_t target_comp) {
    printf("Send command ACK\n");
    mavlink_message_t msg;
    mavlink_msg_command_ack_pack(
            aps_sys_id, aps_comp_id,
            &msg, command, result, 0, 0, target_sys, target_comp);
    uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
    uint32_t send_len = uart_fifo_fill(usb, buf, len);
    if (send_len < len) {
        printf("Drop %d bytes", len - send_len);
    }
    return 0;
}

void parse_mavlink(uint8_t buffer[], int recv_len) {

    for (int i = 0; i < recv_len; i++)
    {
        if (mavlink_parse_char(chan, buffer[i], &rmsg, &rstatus))
        {
            printf("Received message with ID %d, sequence: %d from component %d of system %d\n", rmsg.msgid, rmsg.seq, rmsg.compid, rmsg.sysid);
            // ... DECODE THE MESSAGE PAYLOAD HERE ...
            switch (rmsg.msgid) {
                case MAVLINK_MSG_ID_COMMAND_LONG: {
                    mavlink_command_long_t command;
                    mavlink_msg_command_long_decode(&rmsg, &command);
                    printf("Received command with ID %d, target %d %d\n", 245, command.target_system, command.target_component);

                    switch (command.command) {
                        case MAV_CMD_REQUEST_MESSAGE: {
                            if (command.param1 == MAVLINK_MSG_ID_GIMBAL_MANAGER_INFORMATION) {
                                send_gimbal_manager_info(usb);
                            }
                            break;
                        }
                        case MAV_CMD_DO_GIMBAL_MANAGER_CONFIGURE: {
                            sysid_primary_control = command.param1;
                            compid_primary_control = command.param2;
                            sysid_secondary_control = command.param3;
                            compid_secondary_control = command.param4;
                            break;
                        }
                        default: {
                            send_ack(command.command,
                                    MAV_RESULT_UNSUPPORTED,
                                    rmsg.sysid, rmsg.compid);
                            return;
                        }
                    }

                    // Acknowledge command status
                    send_ack(command.command,
                             MAV_RESULT_ACCEPTED,
                             rmsg.sysid, rmsg.compid);
                    break;
                }
                case MAVLINK_MSG_ID_GIMBAL_MANAGER_SET_ATTITUDE: {
                    mavlink_gimbal_manager_set_attitude_t command;
                    mavlink_msg_gimbal_manager_set_attitude_decode(&rmsg, &command);

                    printf("set_attitude quat (%d, %d, %d, %d)\n",
                           (int) (command.q[0] * 100),
                           (int) (command.q[1] * 100),
                           (int) (command.q[2] * 100),
                           (int) (command.q[3] * 100));
                    printf("%d %d", command.target_system, command.target_component);
                    printf("\n");
                    printf("\n");
                    printf("\n");
                }
            }
        }
    }
}

void heartbeat_timer_cb(struct k_timer *timer_id) {
    send_heartbeat();
}

void status_timer_cb(struct k_timer *timer_id) {
    send_gimbal_manager_status();
}

void attitude_timer_cb(struct k_timer *timer_id) {
    send_gimbal_device_attitude_status();
}

void setup_mavlink_timers() {
    k_timer_init(&heartbeat_timer, heartbeat_timer_cb, NULL);
    k_timer_init(&gimbal_status_timer, status_timer_cb, NULL);
    k_timer_init(&attitude_status_timer, attitude_timer_cb, NULL);

    k_timer_start(&heartbeat_timer, K_MSEC(1000), K_MSEC(1000));
    k_timer_start(&gimbal_status_timer, K_MSEC(200), K_MSEC(200));
    k_timer_start(&attitude_status_timer, K_MSEC(100), K_MSEC(100));
}
