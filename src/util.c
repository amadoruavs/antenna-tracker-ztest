#include <stdio.h>

#include <zephyr.h>
#include <device.h>
#include <logging/log.h>

#include "util.h"

LOG_MODULE_DECLARE(ANTENNA_TRACKER, LOG_LEVEL_DBG);

// only used for debugging, not for final code
void list_devices()
{
    size_t num_devs;
    const struct device *devices;
    num_devs = z_device_get_all_static(&devices);
    LOG_DBG("Number of devices: %d\n", num_devs);
    for (int i = 0;i < num_devs;i++) {
        const struct device dev = devices[i];
        LOG_DBG("Name: %s\n", dev.name);
    }
}

char *now_str(void)
{
    static char buf[16];
    uint32_t now = k_uptime_get_32();
    unsigned int ms = now % MSEC_PER_SEC;
    unsigned int s, min, h;

    now /= MSEC_PER_SEC;
    s = now % 60U;
    now /= 60U;
    min = now % 60U;
    now /= 60U;
    h = now;

    snprintf(buf, sizeof(buf), "%u:%02u:%02u.%03u",
            h, min, s, ms);
    return buf;
}
