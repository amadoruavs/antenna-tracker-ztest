#include <stdio.h>

#include <zephyr.h>
#include <device.h>
#include <logging/log.h>

#include <mavlink/common/mavlink.h>

#include "util.h"
#include "board.h"
#include "imu.h"
#include "usb.h"
#include "mavlink.h"

LOG_MODULE_REGISTER(antenna_tracker, LOG_LEVEL_DBG);

#ifndef CONFIG_MPU6050_TRIGGER
#define IMU_POLL_STACK_SIZE 500
#define IMU_POLL_PRIORITY 0
K_THREAD_STACK_DEFINE(imu_poll_stack_area, IMU_POLL_STACK_SIZE);
struct k_thread imu_poll_thread_data;
k_tid_t imu_poll_tid;
#endif

void main(void)
{
    /* IMU setup */
    const struct device *mpu6050 = device_get_binding(IMU_LABEL);
    if (!mpu6050) {
        LOG_ERR("Failed to initialize device %s\n", IMU_LABEL);
        return;
    }

#ifdef CONFIG_MPU6050_TRIGGER
	int ret;
	ret = setup_mpu6050_trigger(mpu6050);
	if (ret != 0) {
		LOG_ERR("Unable to configure MPU6050 trigger: %d", ret);
		return;
	}
#endif

#ifndef CONFIG_MPU6050_TRIGGER
	imu_poll_tid = k_thread_create(&imu_poll_thread_data, imu_poll_stack_area,
			K_THREAD_STACK_SIZEOF(imu_poll_stack_area),
			imu_poll_thread_entry,
			mpu6050, NULL, NULL,
			IMU_POLL_PRIORITY, 0, K_NO_WAIT);
#endif

    // USB setup
    // TODO: move usb into thread
    /*const struct device *usb = setup_usb_connection("CDC_ACM_0");*/
    /*if (!usb) {*/
        /*printf("Failed to initialize USB %s\n", "CDC_ACM_0");*/
        /*return;*/
    /*}*/

    // PWM setup
    const struct device *pwm2 = device_get_binding("PWM_SERVO");

    if (!pwm2) {
        printf("Failed to initialize PWM_2\n");
        return;
    }

    // mavlink setup
    // sysid + compid setup
    // sysid = any 0-255
    // compid = autopilot, because mavsdk hardcodes it for gimbal control -_-
    /*set_mavlink_id(69, MAV_COMP_ID_GIMBAL);*/
    /*set_mavlink_usb(usb);*/
    /*setup_mavlink_timers();*/
    return;
}
