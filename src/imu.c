/** imu.c
 *
 * This file contains code for interfacing with the Invensense MPU6050
 * inertial measurement unit.
 *
 * @author Kalyan Sriram <kalyan@coderkalyan.com>
 */

#include <stdio.h>
#include <zephyr.h>
#include <device.h>
#include <drivers/sensor.h>
#include <logging/log.h>

#include "imu.h"

LOG_MODULE_REGISTER(imu, LOG_LEVEL_DBG);

#define IMU_STACK_SIZE 2000
#define IMU_PRIORITY 0

extern void imu_thread_entry(void *, void *, void *);

// TODO: find appropriate size for alignment
K_MSGQ_DEFINE(imu_stream, sizeof(struct imu_sample), 3, 32);
K_THREAD_DEFINE(imu_tid, IMU_STACK_SIZE,
		imu_thread_entry, NULL, NULL, NULL,
		IMU_PRIORITY, 0, 0);

const double RTOD = 180 / 3.14159265;

int process_imu(const struct device *dev, struct imu_sample *imu_sample)
{
    int64_t time_now;
    struct sensor_value gyro[3];
    struct sensor_value accel[3];
    struct sensor_value temperature;

    time_now = k_uptime_get();

    int rc = sensor_sample_fetch(dev);
    if (rc == 0) {
        rc = sensor_channel_get(dev, SENSOR_CHAN_ACCEL_XYZ, accel);
    }
    if (rc == 0) {
        rc = sensor_channel_get(dev, SENSOR_CHAN_GYRO_XYZ, gyro);
    }
    if (rc == 0) {
        rc = sensor_channel_get(dev, SENSOR_CHAN_AMBIENT_TEMP, &temperature);
    }

    imu_sample->timestamp = time_now;
    imu_sample->gyro[0] = sensor_value_to_double(&accel[0]);
    imu_sample->gyro[1] = sensor_value_to_double(&accel[1]);
    imu_sample->gyro[2] = sensor_value_to_double(&accel[2]);
    imu_sample->accel[0] = sensor_value_to_double(&accel[0]);
    imu_sample->accel[1] = sensor_value_to_double(&accel[1]);
    imu_sample->accel[2] = sensor_value_to_double(&accel[2]);
    imu_sample->temp = sensor_value_to_double(&temperature);

    return rc;
}

#ifdef CONFIG_MPU6050_TRIGGER
static void handle_mpu6050_trigger(const struct device *dev,
		struct sensor_trigger *trig);

static struct sensor_trigger trigger;
int setup_mpu6050_trigger(const struct device *dev)
{
	trigger = (struct sensor_trigger) {
		.type = SENSOR_TRIG_DATA_READY,
		.chan = SENSOR_CHAN_ALL,
	};
	if (sensor_trigger_set(dev, &trigger,
				handle_mpu6050_trigger) < 0) {
		LOG_ERR("Failed to configure trigger\n");
		return -EIO;
	};

	LOG_DBG("Configured sensor for triggered sampling.\n");
	return 0;
}

static void handle_mpu6050_trigger(const struct device *dev,
        struct sensor_trigger *trig)
{
    struct imu_sample imu_sample;
    int ret = process_imu(dev, &imu_sample);
    if (ret != 0) {
		LOG_ERR("Error processing IMU sample: %d", ret);
		return;
	}

    // we shouldn't run out of space in the message queue,
    // but in case we do, purge until we can insert
    while (k_msgq_put(&imu_stream, &imu_sample, K_NO_WAIT) != 0) {
        LOG_ERR("Dropping IMU samples");
        k_msgq_purge(&imu_stream);
    }
}
#endif /* CONFIG_MPU6050_TRIGGER */

#ifndef CONFIG_MPU6050_TRIGGER
// fallback IMU poll loop for when trigger is not enabled
void imu_poll_thread_entry(void *dev_ptr, void *unused2, void *unused3)
{
	LOG_DBG("Initializing IMU poll thread");

	const struct device *dev = (struct device *) dev_ptr;
	struct imu_sample imu_sample;

	while (1) {
		int ret = process_imu(dev, &imu_sample);
		if (ret != 0) {
			LOG_ERR("Error processing IMU sample: %d", ret);
			break;
		}

		// we shouldn't run out of space in the message queue,
		// but in case we do, purge until we can insert
		while (k_msgq_put(&imu_stream, &imu_sample, K_NO_WAIT) != 0) {
			LOG_ERR("Dropping IMU samples");
			k_msgq_purge(&imu_stream);
		}

		k_sleep(K_MSEC(1));
	}
}

#endif /* !CONFIG_MPU6050_TRIGGER */

void imu_thread_entry(void *unused1, void *unused2, void *unused3)
{
    struct imu_sample imu_sample;
    
    int64_t time_now, time_prev; // in millis
    double time_delta; // in seconds

    double gyro_error[3];
    double accel_error[3];
    double gyro_angle[3], accel_angle[3], angle[3];

    LOG_DBG("Calibrating...");
    for (int i = 0;i < CALIB_NUM_SAMPLES;i++) {
        k_msgq_get(&imu_stream, &imu_sample, K_FOREVER);
        for (int i = 0;i < 3;i++) {
            gyro_error[i] += imu_sample.gyro[i];
            accel_error[i] += imu_sample.accel[i];
        }
    }
    // store the timestamp of the last calibration sample
    // to use for our integration later on
    time_now = imu_sample.timestamp;

    for (int i = 0;i < 3;i++) {
        gyro_error[i] /= (double) CALIB_NUM_SAMPLES;
        accel_error[i] /= (double) CALIB_NUM_SAMPLES;
    }

    LOG_DBG("Done calibrating");

    while (1) {
        k_msgq_get(&imu_stream, &imu_sample, K_FOREVER);

        // apply calibration offset
        for (int i = 0;i < 3;i++) {
            imu_sample.gyro[i] -= gyro_error[i];
            imu_sample.accel[i] -= accel_error[i];
        }

        time_prev = time_now;
        time_now = imu_sample.timestamp;
        time_delta = ((double) time_now - time_prev) / 1000.0;

        // calculate angle from gyro by simply summing
        for (int i = 0;i < 3;i++) {
            gyro_angle[i] += imu_sample.gyro[i] * time_delta * RTOD;
            angle[i] = gyro_angle[i];
            // FIXME: %.3f not working, so round to 3 decimal places
            angle[i] = ((int) (angle[i] * 1000)) / 1000.0;
        }

        /*printf("Data: %.2f %.2f %.2f\r\n", */
                /*imu_sample.gyro[0], imu_sample.gyro[1], imu_sample.gyro[2]);*/
        printf("Angle: %.2f %.2f %.2f\r\n", 
                angle[0], angle[1], angle[2]);
    }
}
