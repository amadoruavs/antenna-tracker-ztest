/** usb.c
 *
 * This file contains code for interfacing with the ground computer
 * via a USB connection.
 *
 * Most functions should be self explanatory.
 *
 * @author Vincent Wang
 */
#include <stdio.h>
#include <string.h>
#include <device.h>
#include <drivers/sensor.h>
#include <logging/log.h>
#include <drivers/uart.h>
#include <usb/usb_device.h>
#include "mavlink.h"

#include "usb.h"

// Register logging module
LOG_MODULE_REGISTER(cdc_acm_echo, LOG_LEVEL_INF);

// Ring buffer seems unneeded
// Add back if needed
// #define RING_BUF_SIZE 1024
// uint8_t usb_ring_buffer[RING_BUF_SIZE];
// struct ring_buf usb_ringbuf;

void usb_interrupt_handler(const struct device *dev, void *user_data)
{
    /* Example interrupt callback for USB data.
     * This will generally not be used except for testing.
     * Echoes back any data sent through USB.
     */
	ARG_UNUSED(user_data);

	while (uart_irq_update(dev) && uart_irq_is_pending(dev)) {
		if (uart_irq_rx_ready(dev)) {
            // Shove input into the ring buffer
			int recv_len, rb_len;
			uint8_t buffer[64];
			recv_len = uart_fifo_read(dev, buffer, sizeof(buffer));
            parse_mavlink(buffer, recv_len);
		}
	}
}


const struct device* setup_usb_connection(char* device_label)
{
    /* Setup a USB CDC ACM connection.
     *
     * @param device_label: ACM device to add (usually CDC_ACM_0).
     * @param interrupt_callback: UART IRQ callback.
     */
    const struct device *dev;
    uint32_t baudrate, dtr = 0U;
    int ret;

    dev = device_get_binding(device_label);
    if (!dev) {
        LOG_ERR("CDC ACM device not found");
        return NULL;
    }

    ret = usb_enable(NULL);
    if (ret != 0) {
        LOG_ERR("Failed to enable USB");
        return NULL;
    }

    LOG_INF("Wait for DTR");

    while (true) {
        uart_line_ctrl_get(dev, UART_LINE_CTRL_DTR, &dtr);
        if (dtr) {
            break;
        } else {
            /* Give CPU resources to low priority threads. */
            k_sleep(K_MSEC(100));
        }
    }

    LOG_INF("DTR set");

    /* They are optional, we use them to test the interrupt endpoint */
    ret = uart_line_ctrl_set(dev, UART_LINE_CTRL_DCD, 1);
    if (ret) {
        LOG_WRN("Failed to set DCD, ret code %d", ret);
    }

    ret = uart_line_ctrl_set(dev, UART_LINE_CTRL_DSR, 1);
    if (ret) {
        LOG_WRN("Failed to set DSR, ret code %d", ret);
    }

    /* Wait 1 sec for the host to do all settings */
    k_busy_wait(1000000);

    ret = uart_line_ctrl_get(dev, UART_LINE_CTRL_BAUD_RATE, &baudrate);
    if (ret) {
        LOG_WRN("Failed to get baudrate, ret code %d", ret);
    } else {
        LOG_INF("Baudrate detected: %d", baudrate);
    }

    uart_irq_callback_set(dev, usb_interrupt_handler);

    /* Enable rx interrupts */
    uart_irq_rx_enable(dev);

    return dev;
}
