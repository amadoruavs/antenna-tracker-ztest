#ifndef __USB_H
#define __USB_H
#include <stdio.h>
#include <string.h>
#include <device.h>
#include <drivers/sensor.h>
#include <logging/log.h>
#include <drivers/uart.h>
#include <sys/ring_buffer.h>
#include <usb/usb_device.h>


void usb_interrupt_handler(const struct device *dev, void *user_data);

const struct device* setup_usb_connection(char* device_label);
#endif // __USB_H
