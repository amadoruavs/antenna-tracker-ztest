#ifndef __APS_MAVLINK_H
#define __APS_MAVLINK_H
#include <stdio.h>
#include <string.h>
#include <zephyr.h>
#include <device.h>
#include <sys/printk.h>
#include <drivers/sensor.h>
#include <logging/log.h>
#include <sys/ring_buffer.h>

void set_mavlink_id(uint8_t sys_id, uint8_t comp_id);
void set_mavlink_usb(const struct device *dev);
uint8_t send_heartbeat();
uint8_t send_gimbal_manager_info();
void setup_mavlink_timers();
void parse_mavlink(uint8_t buffer[], int recv_len);

#endif // __APS_MAVLINK_H
