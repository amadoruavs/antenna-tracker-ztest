#ifndef BOARD_H
#define BOARD_H

#include <zephyr.h>
#include <device.h>

#define IMU_NODE DT_ALIAS(imu)
#if DT_NODE_HAS_STATUS(IMU_NODE, okay)
#define IMU_LABEL DT_LABEL(IMU_NODE)
#else
#error "Unsupported board."
#endif

#endif /* BOARD_H */
